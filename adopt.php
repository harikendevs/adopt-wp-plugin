<?php
/**
 * Plugin Name:       AdOpt - Plugin para LGPD 
 * Description:       Fique adequado à LGPD usando o plugin da AdOpt
 * Version:           1.0.0
 * Author:            AdOpt
 * Author URI:        https://goadopt.io/
 * Text Domain:       goadopt
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * GitHub Plugin URI: https://github.com/g-adamante/adopt-wp.git
 */
 
?>
<?php 

  function adopt_code_register_settings() {
    add_option( 'adopt_code_number', '');
    register_setting( 'adopt_code_options_group', 'adopt_code_number');
  }

  add_action( 'admin_init', 'adopt_code_register_settings' );

?>
<?php
  
  function adopt_code_register_options_page() {
    add_options_page('AdOpt - Configurações LGPD', 'AdOpt - Configurações LGPD', 'manage_options', 'adopt_code_number', 'adopt_code_options_page');
  }
  
  add_action('admin_menu', 'adopt_code_register_options_page');

?>
<?php function adopt_code_options_page()
{
?>
  <div>
  <h2>Configurações do AdOpt</h2>
  <form method="post" action="options.php">
  <?php settings_fields( 'adopt_code_options_group' ); ?>
  <h3>Configure seu plugin AdOpt para ficar adequado à LGPD</h3>
  <table>
  <tr valign="top">
  <th scope="row"><label for="adopt_code_number">Código AdOpt</label></th>
  <td><input type="tel" id="adopt_code_number" name="adopt_code_number" value="<?php echo get_option('adopt_code_number'); ?>" /></td>
  <p>Encontre seu código no <a href="https://app.goadopt.io/dashboard">Dashboard da AdOpt</a>
  </tr>
  </table>
  <?php  submit_button(); ?>
  </form>
  </div>
<?php
} 
?>
<?php 

function adopt_footer() {

    echo "<script src='//tag.goadopt.io/injector.js?website_code=".get_option('adopt_code_number')."' class='adopt-injector'>
    </script>";
    }

    add_action( 'wp_footer', 'adopt_footer' );

?>